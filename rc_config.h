/*  OSSP rc - Run-Command Processor
**  Copyright (c) 2002-2003 Ralf S. Engelschall
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland GmbH
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP rc, a portable run-command processor
**  which can be found at http://www.ossp.org/pkg/lib/rc/
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  rc_config.h: Run-Command Processor ISO C public header file
*/

#ifndef __OSSPRC_CONFIG_H__
#define __OSSPRC_CONFIG_H__

/* Available options and their values */
#define RC_HEADVAL   0  /* Unused         */
#define RC_USE_VAL   1  /* Usage          */
#define RC_DBG_VAL   2  /* Debug          */
#define RC_VER_VAL   3  /* Version        */
#define RC_EVL_VAL   4  /* Eval           */
#define RC_HLP_VAL   5  /* Help           */
#define RC_INF_VAL   6  /* Info           */
#define RC_LBL_VAL   7  /* Label          */
#define RC_PRN_VAL   8  /* Print          */
#define RC_PAR_VAL   9  /* Parse names    */
#define RC_SIL_VAL  10  /* Silent         */
#define RC_RAW_VAL  11  /* Raw            */
#define RC_VRB_VAL  12  /* Verbose        */
#define RC_EXC_VAL  13  /* Exec           */
#define RC_LOC_VAL  14  /* Locations      */
#define RC_CNF_VAL  15  /* Conf file      */
#define RC_FNC_VAL  16  /* Func file      */
#define RC_QRY_VAL  17  /* Query          */
#define RC_TMP_VAL  18  /* Temp dir       */
#define RC_OWN_VAL  19  /* User name      */
#define RC_GRP_VAL  20  /* Group name     */
#define RC_MSK_VAL  21  /* Umask          */
#define RC_ASS_VAL  22  /* Assign regex   */
#define RC_DEF_VAL  23  /* Label regex    */
#define RC_REF_VAL  24  /* Ref regex      */
#define RC_PRM_VAL  25  /* Params regex   */
#define RC_TRM_VAL  26  /* Terminal regex */
#define RC_NCF_VAL  27  /* Config name    */
#define RC_CMN_VAL  28  /* Common name    */
#define RC_DFL_VAL  29  /* Default name   */
#define RC_ERR_VAL  30  /* Error name     */

#define RC_NUMOPTS  31  /* Increment with each option addition */

#endif /* __OSSPRC_CONFIG_H__ */
