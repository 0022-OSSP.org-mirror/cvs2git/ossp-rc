/*
**  rc_version.c -- Version Information for OSSP rc (syntax: C/C++)
**  [automatically generated and maintained by GNU shtool]
*/

#ifdef _RC_VERSION_C_AS_HEADER_

#ifndef _RC_VERSION_C_
#define _RC_VERSION_C_

#define RC_VERSION 0x007203

typedef struct {
    const int   v_hex;
    const char *v_short;
    const char *v_long;
    const char *v_tex;
    const char *v_gnu;
    const char *v_web;
    const char *v_sccs;
    const char *v_rcs;
} rc_version_t;

extern rc_version_t rc_version;

#endif /* _RC_VERSION_C_ */

#else /* _RC_VERSION_C_AS_HEADER_ */

#define _RC_VERSION_C_AS_HEADER_
#include "rc_version.c"
#undef  _RC_VERSION_C_AS_HEADER_

rc_version_t rc_version = {
    0x007203,
    "0.7.3",
    "0.7.3 (11-Jul-2003)",
    "This is OSSP rc, Version 0.7.3 (11-Jul-2003)",
    "OSSP rc 0.7.3 (11-Jul-2003)",
    "OSSP rc/0.7.3",
    "@(#)OSSP rc 0.7.3 (11-Jul-2003)",
    "$Id: rc_version.c,v 1.5 2003/07/11 14:05:44 ms Exp $"
};

#endif /* _RC_VERSION_C_AS_HEADER_ */

