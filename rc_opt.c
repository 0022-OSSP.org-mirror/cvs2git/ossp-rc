/*  OSSP rc - Run-Command Processor
**  Copyright (c) 2002-2003 Ralf S. Engelschall
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland GmbH
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP rc, a portable run-command processor
**  which can be found at http://www.ossp.org/pkg/lib/rc/
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  rc_opt.c: Run-Command Processor ISO C source file
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "rc.h"

/* FIXME mlelstv -- why Construct/Destruct vs. New/Delete ? */

/***************************************
* optConstruct(rc_opt_t **)            *
* Construct an option                  *
***************************************/
rc_return_t optConstruct(rc_opt_t **ppOption)
{
    assert(*ppOption == NULL);              /* Error if constructed already */
    *ppOption = malloc(sizeof(**ppOption)); /* Allocate an option instance  */
    if (!*ppOption)
        return(RC_THROW(RC_ERR_MEM));

    (*ppOption)->szName = NULL;             /* We don't know how much to    */
    (*ppOption)->szVal  = NULL;             /* allocate, so do it later     */

    return(RC_THROW(RC_OK));
}

/***************************************
* optGetXXXX(rc_opt_t *, char **)      *
* Option accessors                     *
***************************************/
rc_return_t optGetname(rc_opt_t *pOption, char **pszName)
{
    assert(pOption && pOption->szName);
    /* FIXME mlelstv -- why non-shared read ? */
    if (!(*pszName = strdup(pOption->szName)))  /* Get name from option     */
        return(RC_THROW(RC_ERR_MEM));
    else
        return(RC_THROW(RC_OK));
}

rc_return_t optGetval(rc_opt_t *pOption, char **pszVal)
{
    assert(pOption && pOption->szVal);
    /* FIXME mlelstv -- why non-shared read ? */
    if (!(*pszVal = strdup(pOption->szVal)))    /* Get value from option    */
        return(RC_THROW(RC_ERR_MEM));
    else
        return(RC_THROW(RC_OK));
}

/***************************************
* optSetXXXX(rc_opt_t *, const char *) *
* Option accessors                     *
***************************************/
rc_return_t optSetname(rc_opt_t *pOption, const char *kszName)
{
    if (pOption->szName)                        /* Guard against leaks  */
        free(pOption->szName);                  /* if resetting name    */
    if (!(pOption->szName = strdup(kszName)))   /* Set name of option   */
        return(RC_THROW(RC_ERR_MEM));
    else
        return(RC_THROW(RC_OK));
}

rc_return_t optSetval(rc_opt_t *pOption, const char *kszVal)
{
    if (pOption->szVal)                         /* Guard against leaks  */
        free(pOption->szVal);                   /* if resetting value   */
    if (!(pOption->szVal = strdup(kszVal)))     /* Set value of option  */
        return(RC_THROW(RC_ERR_MEM));
    else
        return(RC_THROW(RC_OK));
}

/***************************************
* optDestruct(rc_opt_t **)             *
* Destruct a command line option       *
***************************************/
rc_return_t optDestruct(rc_opt_t **ppOption)
{
    assert(*ppOption);              /* Error if not constructed */

    if ((*ppOption)->szName)        /* Might not be mallocd yet */
        free((*ppOption)->szName);
    if ((*ppOption)->szVal)         /* Might not be mallocd yet */
        free((*ppOption)->szVal);
    free(*ppOption);                /* Deallocate option and    */
    *ppOption = NULL;               /* clear its reference      */

    return(RC_THROW(RC_OK));
}
