#! /bin/sh
#  OSSP rc - Run-Command Processor
#  Copyright (c) 2002-2003 Ralf S. Engelschall
#  Copyright (c) 2002-2003 Cable & Wireless Deutschland GmbH
#  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
#
#  This file is part of OSSP rc, a portable run-command processor
#  which can be found at http://www.ossp.org/pkg/lib/rc/
#
#  Permission to use, copy, modify, and distribute this software for
#  any purpose with or without fee is hereby granted, provided that
#  the above copyright notice and this permission notice appear in all
#  copies.
#
#  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
#  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
#  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#  SUCH DAMAGE.
#
#  test_rc.sh: Run-Command Processor test script

if [ -d $MOPKG/etc ]; then
    RCBASE=$MOPKG/etc
elif [ -d /sw/etc ]; then
    RCBASE=/sw/etc
elif [ -d /cw/etc ]; then
    RCBASE=/cw/etc
elif [ -d /usr/opkg/etc ]; then
    RCBASE=/usr/opkg/etc
fi

echo "*** Test short options, should fail for false combination usage ***"
echo "./rc -Dvexpf ./rc_test/rcfuncs -islrvc rc.conf -q somevalue -t /tmp -L rc_test samba search"
./rc -Dvexpf ./rc_test/rcfuncs -ilrvc rc.conf -q somevalue -t /tmp -L rc_test samba search
echo; echo "<Press enter to continue>"
read dummy;clear

echo "*** Test short options, should succeed ***"
echo "./rc -Dpf ./rc_test/rcfuncs -irt /tmp -L rc_test -c rc.conf samba search finish"
./rc -Dpf ./rc_test/rcfuncs -irt /tmp -L rc_test -c rc.conf samba search finish
echo; echo "<Press enter to continue>"
read dummy;clear
echo "*** Test and evaluate short options, should succeed ***"
echo "eval \`./rc -Def ./rc_test/rcfuncs -irt /tmp -L rc_test -c rc.conf samba search finish\`"
eval `./rc -Def ./rc_test/rcfuncs -irt /tmp -L rc_test -c rc.conf samba search finish`
echo; echo "<Press enter to continue>"
read dummy;clear
echo "*** Test short options with verbose, should succeed ***"
echo "./rc -Dpf ./rc_test/rcfuncs -irvt /tmp -L rc_test -c rc.conf samba search finish"
./rc -Dpf ./rc_test/rcfuncs -irvt /tmp -L rc_test -c rc.conf samba search finish
echo; echo "<Press enter to continue>"
read dummy;clear
#echo "./rc -Derv -L ./rc_test/rc.%{RCFILE:s/^all$/*/} -c ./rc.conf -f ./rc_test/rcfuncs -t /tmp openssh stop sleep=4 start daily minsize=2097152"
#./rc -Derv -L ./rc_test/rc.d/rc.%{RCFILE:s/^all$/*/} -c ./rc.conf -f ./rc_test/rcfuncs -t /tmp openssh stop sleep=4 start daily minsize=2097152
#echo; echo "<Press enter to continue>"
#read dummy;clear

echo "*** Test some long options, should fail for false combination usage ***"
echo "./rc --verbose --silent --print --NameConfig config --locate rc_test --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" zebra sleep=6 start test"
./rc --verbose --silent --print --NameConfig config --locate rc_test --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" zebra sleep=6 start test
echo; echo "<Press enter to continue>"
read dummy;clear
echo "*** Test some long options, should fail for false combination usage ***"
echo "./rc --query myvar --print --exec --locate rc_test --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" zebra start test"
./rc --query myvar --print --exec --locate rc_test --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" zebra start test
echo; echo "<Press enter to continue>"
read dummy;clear
echo "*** Test some long options, should fail for false combination usage ***"
echo "./rc --info --print --eval --silent --ParseSectionDef \"^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>\" --query format --locate ./rc_test/ pam test"
./rc --info --print --eval --silent --ParseSectionDef "^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>" --query format --locate ./rc_test/ pam test
echo; echo "<Press enter to continue>"
read dummy;clear

echo "*** Test minimal set of long options, should succeed ***"
echo "./rc --locate rc_test --ParseSectionDef \"^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>\" rsyncd go"
./rc --locate rc_test --ParseSectionDef "^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>" rsyncd go
echo; echo "<Press enter to continue>"
read dummy;clear

echo "*** Test minimal set of long options, should succeed ***"
echo "./rc -L rc_test --ParseSectionDef \"^%([a-zA-Z][a-zA-Z0-9]*)\s*(-[a-zA-Z]\s*\S+)*\n(.+?)\n^%\" zebra test"
./rc -L rc_test --ParseSectionDef "^%([a-zA-Z][a-zA-Z0-9]*)\s*(-[a-zA-Z]\s*\S+)*\n(.+?)\n^%" zebra test
echo; echo "<Press enter to continue>"

read dummy;clear
echo "*** Test minimal set of long options, should succeed ***"
echo "./rc --version --ParseSectionDef \"^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>\" rsyncd nothing matters but the version"
./rc --version --ParseSectionDef "^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>" rsyncd nothing matters but the version
echo; echo "<Press enter to continue>"
read dummy;clear
echo "*** Test minimal set of long options, should succeed ***"
echo "./rc --exec --conf $RCBASE/rc.conf --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" --locate $RCBASE/rc.d:./rc_test:/etc/rc --query format all barf test"
./rc --exec --conf rc.conf --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" --locate rc_test --query format all barf test
echo; echo "<Press enter to continue>"
read dummy;clear

# Some examples using fake rc files and REAL ONES AS WELL, use with care!
echo "******************************************************"
echo "*The following test will execute test suite code,    *"
echo "*and code ALSO FROM YOUR REAL OPENPKG RC FILES!!!    *"
echo "*If this concerns you, then press Ctrl-C now to exit.*"
echo "******************************************************"
echo; echo "<Press enter to continue>"
read dummy;clear
echo "./rc --conf $RCBASE/rc.conf --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" --locate $RCBASE/rc.d:./rc_test:/etc/rc --query all barf test"
./rc --conf $RCBASE/rc.conf --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" --locate $RCBASE/rc.d:./rc_test:/etc/rc --query all barf test
echo; echo "<Press enter to continue>"
#read dummy;clear

# FIXME these cases are not handled yet by our configuration FIXME #
exit
#echo; echo "./rc --debug --version"
#./rc --debug --version
#echo; echo "./rc --debug --help"
#./rc --debug --help
#echo; echo "./rc --debug --info --verbose"
#./rc --debug --info --verbose
#echo; echo "./rc --conf /etc/rc.conf --debug --info --raw"
#./rc --conf /etc/rc.conf --debug --info --raw
#echo; echo "./rc --conf $RCBASE/rc.conf --tmp /tmp --debug --labels rsyncd"
#./rc --conf $RCBASE/rc.conf --tmp /tmp --debug --labels rsyncd
# FIXME these cases are not handled yet by our configuration FIXME #

#echo; echo "./rc --func rc_test/rcfuncs --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" --verbose openssh stop sleep=4 start"
#./rc --func rc_test/rcfuncs --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" --verbose openssh stop sleep=4 start
#read dummy;clear
#echo; echo "./rc --func ./rc_test/rcfuncs --info --eval uucp restart"
#./rc --func ./rc_test/rcfuncs --info --eval uucp restart
#read dummy;clear
#echo; echo "./rc --conf rc_test/myrc.conf --func ./rc_test/rcfuncs --tmp /tmp --ParseSectionDef \"^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>\" uucp start"
#./rc --conf rc_test/myrc.conf --func ./rc_test/rcfuncs --tmp /tmp --ParseSectionDef "^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>" uucp start
#read dummy;clear
#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs --tmp /tmp --debug --ParseSectionDef \"^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>\" ntp sync"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs --tmp /tmp --debug --ParseSectionDef "^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>" ntp sync
#read dummy;clear
#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs --tmp /tmp --debug --silent --ParseSectionDef \"^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>\" ralf feed suppe"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs --tmp /tmp --debug --silent --ParseSectionDef "^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>" ralf feed suppe
#read dummy;clear
#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L rc_test --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" --tmp /tmp --silent samba search finish destroy"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L rc_test --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" --tmp /tmp --silent samba search finish destroy
#read dummy;clear

#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L ./rc_test --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" --tmp /tmp --silent all feed suppe"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L ./rc_test --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" --tmp /tmp --silent all feed suppe
#read dummy;clear

#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" --tmp /tmp --silent --debug all config barf gag"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" --tmp /tmp --silent --debug all config barf gag
#read dummy;clear

#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" --tmp /tmp --silent --debug --eval all config start"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" --tmp /tmp --silent --debug --eval all config start
#read dummy;clear

#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" --tmp /tmp --silent --debug --exec all config start"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" --tmp /tmp --silent --debug --exec all config start

#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef \"%(.+)\" --tmp /tmp --silent --print --debug pam info"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef "%(.+)" --tmp /tmp --silent --print --debug pam info
#read dummy;clear

#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef \"%(.+)\" --tmp /tmp --silent --debug apache config start"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L $RCBASE/rc.d --ParseSectionDef "%(.+)" --tmp /tmp --silent --debug apache config start
#read dummy;clear

#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L ./rc_test/ --ParseSectionDef \"^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>\" --tmp /tmp --silent --debug all config start"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L ./rc_test/ --ParseSectionDef "^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>" --tmp /tmp --silent --debug all config start
#read dummy;clear

#echo; echo "./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L ./rc_test/ --ParseSectionDef \"^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>\" --tmp /tmp --silent --debug pam info"
#./rc --conf ./rc_test/myrc.conf --func ./rc_test/rcfuncs -L ./rc_test/ --ParseSectionDef "^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>" --tmp /tmp --silent --debug pam info
#read dummy;clear

#echo; echo "./rc --conf ./rc_test/myrc.conf --func $RCBASE/rc.func -L $RCBASE/rc.d/ --ParseSectionDef \"^%(\w+)[ \t]*(.*?)\n(.*?)^$\" --tmp /tmp --silent --debug dhcpd start"
#./rc --conf ./rc_test/myrc.conf --func $RCBASE/rc.func -L $RCBASE/rc.d/ --ParseSectionDef "^%(\w+)[ \t]*(.*?)\n(.*?)^$" --tmp /tmp --silent --debug dhcpd start
#read dummy;clear

#echo; echo "./rc --func ./rc_test/rcfuncs --exec --ParseSectionDef \"^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>\" -L ./rc_test pam autre"
#./rc --func ./rc_test/rcfuncs --exec --ParseSectionDef "^<(.+?)[ \t]*(.*?)>\n*(.*?)^<\/\1>" -L ./rc_test pam autre
#read dummy;clear

#echo; echo "./rc --func ./rc_test/rcfuncs -L ./rc_test/ dhcpd stop"
#./rc --func ./rc_test/rcfuncs -L ./rc_test/ dhcpd stop
#read dummy;clear

#echo; echo "./rc --exec --func ./rc_test/rcfuncs -L ./rc_test/ dhcpd gotest"
#./rc --exec --func ./rc_test/rcfuncs -L ./rc_test/ dhcpd gotest
#read dummy;clear

# Next milestone
#RequireOwner
#RequireGroup
#RequireUmask
#ParseEnvAss
#ParseSectionDef
#ParseSectionRef
#ParseSectionParam
#ParseTerminal
#NameConfig
#NameCommon
#NameDefault
#NameError
