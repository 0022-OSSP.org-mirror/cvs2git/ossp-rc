/*  OSSP rc - Run-Command Processor
**  Copyright (c) 2002-2003 Ralf S. Engelschall
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland GmbH
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP rc, a portable run-command processor
**  which can be found at http://www.ossp.org/pkg/lib/rc/
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  rc.h: Run-Command Processor ISO C public header file
*/

#ifndef __OSSPRC_H__
#define __OSSPRC_H__

/* compensate for poor standard environments */
#ifndef NULL
#define NULL (void *)0
#endif

#ifndef FALSE
#define FALSE (0)
#endif

#ifndef TRUE
#define TRUE (!FALSE)
#endif

/* Return and error code macro helpers */
#define WARNING(scode)    (scode >= RC_WRN_0)
#define FAILED(scode)    ((scode >= RC_ERR_0) && !WARNING(scode))
#define SUCCEEDED(scode) ((scode == RC_OK)    ||  WARNING(scode))

#include "rc_private.h"

/* Rc return codes */
typedef enum {
    RC_OK      = 0,   /* Success                  */
    RC_ERR_0   = 1,   /* Error base               */
    RC_ERR_USE = 2,   /* Usage error              */
    RC_ERR_MEM = 3,   /* Memory error             */
    RC_ERR_SYS = 4,   /* System error (see errno) */
    RC_ERR_IO  = 5,   /* Input/output error       */
    RC_ERR_INT = 6,   /* Internal error           */
    RC_ERR_FNC = 7,   /* Func file not found      */
    RC_ERR_LOC = 8,   /* Location dir not found   */
    RC_ERR_TMP = 9,   /* Temporary dir not found  */
    RC_ERR_RCF = 10,  /* Rc file not found        */
    RC_ERR_TRM = 11,  /* Request to terminate app */
    RC_ERR_CFG = 12,  /* Config or option failure */
    RC_ERR_ROOT = 13, /* Process must run as root */
    RC_WRN_0   = 14,  /* Warning base             */
    RC_WRN_OWR = 15,  /* Overwrite warning        */
    RC_WRN_NUL = 16   /* NULL pointer warning     */
} rc_return_t;

/* Config function prototypes */
rc_return_t configNew(void);
rc_return_t configLoad(int, const char **);
rc_return_t configInfo(void);
rc_return_t configVerify(void);
rc_return_t configDefaults(void);
rc_return_t configDelete(void);

/* Config accessor prototypes */
const char *configGetval(rc_opt_t);
const char *configGetname(rc_opt_t);
const char *configGetrcfile(void);
const char **configGetsecs(void);

/* Command line function prototypes */
rc_cliopt_t *clioptNew(void);
rc_return_t clioptPrintusage(rc_cliopt_t *);
rc_return_t clioptProcess(rc_cliopt_t *, int, const char *);
rc_return_t clioptParseopts(rc_cliopt_t *, int, const char **);
rc_return_t clioptParseargs(rc_cliopt_t *);
rc_return_t clioptDelete(rc_cliopt_t *);

/* Command line accessor prototypes */
const char *clioptGetval(rc_cliopt_t *, rc_opt_t);
const char *clioptGetrcfile(rc_cliopt_t *);
const char **clioptGetsecs(rc_cliopt_t *);
rc_return_t clioptSetval(rc_cliopt_t *, rc_opt_t, const char *);
rc_return_t clioptSetrcfile(rc_cliopt_t *, const char *);
rc_return_t clioptSetsecs(rc_cliopt_t *, const char **);

/* List function prototypes */
rc_list_t *listNew(void);
rc_return_t listDelete(rc_list_t *);
rc_file_t *listGetrcfile(rc_list_t *, const char *);
rc_return_t listPopulate(rc_list_t *, const char *);

/* Script function prototypes */
rc_script_t *scriptNew(void);
rc_script_t *scriptCopy(rc_script_t *);
rc_return_t scriptAdd(rc_script_t *, rc_script_t *);
rc_return_t scriptnAppend(rc_script_t *, const char *, size_t);
rc_section_t *scriptSection(rc_script_t *, const char *);
rc_return_t scriptDump(rc_script_t *);
rc_return_t scriptWrite(rc_script_t *, const char *);
const char *scriptGetdata(rc_script_t *);
rc_return_t scriptSetdata(rc_script_t *, const char *);
rc_return_t scriptDelete(rc_script_t *);

/* Rcfile function prototypes */
rc_file_t  *rcfileNew(const char *);
rc_return_t rcfileParse(rc_file_t *);
rc_section_t *rcfileGetsec(rc_file_t *, const char *);
rc_return_t rcfileAppendsec(rc_file_t *, rc_section_t *);
const char *rcfileGetname(rc_file_t *);
rc_return_t rcfileSetname(rc_file_t *, const char *);
short rcfileExists(rc_file_t *);
rc_return_t rcfileDelete(rc_file_t *);

/* Section function prototypes */
rc_section_t *sectionNew(const char *);
rc_section_t *sectionCopy(rc_section_t *);
const int sectionGetpri(rc_section_t *);
const int sectionGetuid(rc_section_t *);
const char *sectionGetname(rc_section_t *);
const char *sectionGetparent(rc_section_t *);
const char *sectionGetlabel(rc_section_t *);
const char *sectionGetlogin(rc_section_t *);
rc_script_t *sectionGetscript(rc_section_t *);
const char *sectionGetdata(rc_section_t *);
size_t sectionGetlen(rc_section_t *);
rc_return_t sectionSetpri(rc_section_t *, long);
rc_return_t sectionSetuid(rc_section_t *, long);
rc_return_t sectionSetname(rc_section_t *, const char *);
rc_return_t sectionSetparent(rc_section_t *, const char *);
rc_return_t sectionSetlabel(rc_section_t *, const char *);
rc_return_t sectionSetlogin(rc_section_t *, const char *);
rc_return_t sectionSetdata(rc_section_t *, const char *);
rc_return_t sectionSetndata(rc_section_t *, const char *, size_t);
rc_return_t sectionDump(rc_section_t *);
rc_return_t sectionWrite(rc_section_t *, const char *);
rc_return_t sectionDelete(rc_section_t *);

/* Processor function prototypes */
rc_proc_t *procNew(void);
rc_return_t procDelete(rc_proc_t *);
rc_return_t procPopulate(rc_proc_t *);
rc_return_t procRun(rc_proc_t *);
rc_return_t procEval(rc_proc_t *);
rc_return_t procExec(rc_proc_t *);
rc_return_t procPrint(rc_proc_t *);
rc_return_t procParse(rc_proc_t *);

/* Utility (nonbounded) function prototypes */
char *strErr(rc_return_t);
char **vectorCopy(const char **);
short vectorCount(const char **);
rc_return_t vectorDel(char **);
int priCompare(const void *, const void *);

/* Other function prototypes */
void rcError(ex_t);

/*rc_result_t rc_Err              (void);
rc_result_t rc_Warn             (void);
rc_result_t rc_PathAppend       (int bExists, char **pszPaths);
rc_result_t rc_PathPrepend      (int bExists, char **pszPaths);
rc_result_t rc_EnvDel           (char *szVar, char **pszVals);
rc_result_t rc_QueryVar         (char *szVar, char **pszVal);
rc_result_t rc_ServiceState     (char *szService, char **pszServState);
rc_result_t rc_TmpDirGen        (char *szProgram);
rc_result_t rc_TmpDirFile       (char *szProgram, char *szFilename, *szVar);
rc_result_t rc_TmpDirDel        (char *szProgram);*/

#endif /* __OSSPRC_H__ */
