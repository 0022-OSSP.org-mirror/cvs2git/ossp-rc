/*  OSSP rc - Run-Command Processor
**  Copyright (c) 2002-2003 Ralf S. Engelschall
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland GmbH
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP rc, a portable run-command processor
**  which can be found at http://www.ossp.org/pkg/lib/rc/
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  rc_error.c: Run-Command Processor ISO C source file
*/

#include <stdlib.h>    /* Just for calling 'exit(3)' once */
#include "rc.h"        /* Public interfaces               */
#include "rc_config.h" /* Interface to the configuration  */
#include "rc_const.h"  /* Error message string constants  */


/************************************************
* rcError(ex_t)                                 *
* Main rc error handler                         *
************************************************/
void rcError(ex_t Localerr)
{
    /* FIXME mlelstv -- looks like a case for a switch (pun intended) */

    if (FAILED((rc_return_t)Localerr.ex_value)) { /* Error condition */
        if ((rc_return_t)Localerr.ex_value == RC_ERR_TRM)
            exit(0);
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_USE) {
            fprintf(stderr, RC_ERRTXT_USE);
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_MEM) {
            fprintf(stderr, RC_ERRTXT_MEM);
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_SYS) {
            fprintf(stderr, RC_ERRTXT_SYS);
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_IO) {
            fprintf(stderr, RC_ERRTXT_IO);
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_INT) {
            fprintf(stderr, RC_ERRTXT_INT);
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_FNC) {
            fprintf(stderr, RC_ERRTXT_FNC, \
                configGetval(RC_FNC_VAL));
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_LOC) {
            fprintf(stderr, RC_ERRTXT_LOC, \
                configGetval(RC_LOC_VAL));
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_TMP) {
            fprintf(stderr, RC_ERRTXT_TMP, \
                configGetval(RC_TMP_VAL));
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_RCF) {
            fprintf(stderr, RC_ERRTXT_RCF);
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_CFG) {
            fprintf(stderr, RC_ERRTXT_CFG);
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else if ((rc_return_t)Localerr.ex_value == RC_ERR_ROOT) {
            fprintf(stderr, RC_ERRTXT_ROOT);
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }
        else {
            fprintf(stderr, RC_ERRTXT_GEN, (char *)Localerr.ex_class);
            if (configGetval(RC_DBG_VAL))
                fprintf(stderr, RC_ERRTXT_ALL, \
                    Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
        }

        /* Return a corresponding failure code in every case */
        exit((int)Localerr.ex_value);
    }
#ifdef DEBUG
    else        /* Warning condition */
        fprintf(stderr, "    Warning condition of class '%s',\n    exception %d in %s:%s():%d.\n", (char *)Localerr.ex_class, (int)Localerr.ex_value, Localerr.ex_file, Localerr.ex_func, Localerr.ex_line);
#else
    else        /* Warning condition */
        RC_NOP; /* No operation on warnings */
#endif
}

