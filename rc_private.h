/*  OSSP rc - Run-Command Processor
**  Copyright (c) 2002-2003 Ralf S. Engelschall
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland GmbH
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP rc, a portable run-command processor
**  which can be found at http://www.ossp.org/pkg/lib/rc/
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  rc_private.h: Run-Command Processor ISO C private API header file
*/

#ifndef __OSSPRC_PRIVATE_H__
#define __OSSPRC_PRIVATE_H__

#ifdef HAVE_CONFIG_H
#include "ac_config.h"
#endif

/* Assertion and tracing support */
#include <assert.h>
#ifndef DEBUG
#define NDEBUG
#define TRACE(str)  ((void)0)
#define TRACEL(num) ((void)0)
#else
#define TRACE(str)  fprintf(stderr, "%s:%d: %s\n", __FILE__, __LINE__, str)
#define TRACEL(num) fprintf(stderr, "%s:%d: %ld\n", __FILE__, __LINE__, (long int)num)
#endif

/* Pointer and string manipulation constants */
#define RC_SEC_SUB          1
#define RC_GOOD_MEASURE     8
#define RC_EXEC_MAXARGS     8
#define RC_READ_BUFSIZE  1024
#define RC_WRITE_BUFSIZE 1024
#define RC_STR_ID  "OSSP rc"                        /* APPID for OSSP rc */
#define RC_UID_ID 0xf8a1845c55e6449481176f6e9cea34b /* UUID for OSSP rc  */

/* Intentional no operation */
#define RC_NOP ((void)0)

/* Memory debugging support */
#if defined(HAVE_DMALLOC_H) && defined(WITH_DMALLOC)
#include "dmalloc.h"
#endif

/* Define the ability to throw OSSP ex exceptions       */
#include "ex.h" /* OSSP ex exception handling library   */
#define RC_THROW(rv) \
    ((rv) != RC_OK && (ex_catching && !ex_shielding) \
     ? (ex_throw(RC_STR_ID, NULL, (rv)), (rv)) : (rv))

typedef int rc_opt_t;   /* For use with RC_XXX_VAL definitions  */

/* Cliopt class */
typedef struct {
    char  **m_pszOptuples;
    char  *m_szRcfile;
    char **m_pszSecs;
} rc_cliopt_t;

/* Configuration class */
typedef struct {
    rc_cliopt_t *pCliopts;
} rc_config_t;

/* Script type */
typedef char * rc_script_t;

/* Section type */
typedef struct {
    int m_nPri;
    int m_nUid;
    char *m_szParent;
    char *m_szName;
    char *m_szLogin;
    rc_script_t *m_pData;
} rc_section_t;

/* Rcfile class */
typedef struct {
    short m_nSecs;
    char *m_szName;
    rc_section_t **m_ppSecvec;
} rc_file_t;

/* List class (can be implemented as array) */
typedef struct {
    short m_nFiles;
    rc_file_t **m_ppFilevec;
} rc_list_t;

/* Processor class */
typedef struct {
    rc_script_t *m_pScriptfunc;
    rc_script_t *m_pScriptcnf;
    rc_list_t *m_pList;
} rc_proc_t;

#endif /* __OSSPRC_PRIVATE_H__ */

