/*  OSSP rc - Run-Command Processor
**  Copyright (c) 2002-2003 Ralf S. Engelschall
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland GmbH
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP rc, a portable run-command processor
**  which can be found at http://www.ossp.org/pkg/rc/
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  rc_const.h: Run-Command Processor ISO C public header file
*/

#ifndef __OSSPRC_CONST_H__
#define __OSSPRC_CONST_H__

/* Generic text, should include newline termination */
#define RC_LST_TEXT "OSSP rc:Failure, please enter just one rcfile name and at least one section label.\n"
#define RC_EEP_TEXT "OSSP rc:Failure, the exec, eval, and print options may not be combined.\n"
#define RC_SLO_TEXT "OSSP rc:Failure, the silent option may not be combined with output options.\n"
#define RC_RUT_TEXT "OSSP rc:Failure, failed to set the user id. Please become the root user and try again.\n"
#define RC_SUM_TEXT "OSSP rc:Info, option argument summary.\n"

/* Generic text, should not include newline termination */
#define RC_VST_TEXT "# Start of run command script operations."
#define RC_EVF_TEXT "# Evaluating code from functions file."
#define RC_EVN_TEXT "# Evaluating section %s of rcfile %s."
#define RC_EXF_TEXT "# Executing code from functions file."
#define RC_EXN_TEXT "# Executing section %s of rcfile %s."
#define RC_PNF_TEXT "# Printing code from functions file."
#define RC_PRN_TEXT "# Printing section %s of rcfile %s."

/* Option descriptions used with popt, should not include termination */
#define RC_USE_DESC "Print a short usage summary, then exit."
#define RC_DBG_DESC "Don't remove temporary files, and write debug messages to stderr."
#define RC_VER_DESC "Print the version and copyright, then exit."
#define RC_EVL_DESC "Output the command(s) in a format suitable for shell evaluation, but do not run them."
#define RC_HLP_DESC "Print this help, then exit."
#define RC_INF_DESC "Print a comprehensive summary of the rc environment."
#define RC_LBL_DESC "Learn what section labels a rcfile has."
#define RC_PRN_DESC "Output the command(s) in a format suitable for human reading, but do not run them."
#define RC_PAR_DESC "Parse the run command names along with each of their matching section names."
#define RC_SIL_DESC "Be silent, and disable output to stdout."
#define RC_RAW_DESC "Output text using no terminal control sequences."
#define RC_VRB_DESC "Output text verbosely."
#define RC_EXC_DESC "Execute the commands through an interpreter."

#define RC_LOC_DESC "Specifiy the location(s) to search for rcfiles, and ignore parts according to the regex."
#define RC_CNF_DESC "Specify the location(s) of the configuration files."
#define RC_FNC_DESC "Specify the single location of the extra functions file."
#define RC_QRY_DESC "Query the effective value of configuration variables."
#define RC_TMP_DESC "Specify the single location for temporary files."

#define RC_OWN_DESC "Username used to verify run command rights at runtime."
#define RC_GRP_DESC "Group name used to verify run command rights at runtime."
#define RC_MSK_DESC "Umask used to verify run command rights at runtime."
#define RC_ASS_DESC "Regex used to match name value assignments in a rcfile."
#define RC_DEF_DESC "Regex used to match a section label in a rcfile."
#define RC_REF_DESC "Regex used to match a section reference in a rcfile."
#define RC_PRM_DESC "Regex used to match the parameter(s) of a section label."
#define RC_TRM_DESC "Regex used to match the terminal(s) in a rcfile."
#define RC_NCF_DESC "Name of the config section in a rcfile."
#define RC_CMN_DESC "Name of the common section in a rcfile."
#define RC_DFL_DESC "Name of the default section in a rcfile."
#define RC_ERR_DESC "Name of the error section in a rcfile."

/* Error messages with newline termination */
#define RC_ERRTXT_ALL  "Error condition in %s:%s():%d.\n"
#define RC_ERRTXT_USE  "OSSP rc:Failure, API usage is wrong.\n"
#define RC_ERRTXT_MEM  "OSSP rc:Failure, memory allocation.\n"
#define RC_ERRTXT_SYS  "OSSP rc:Failure, underlying system.\n"
#define RC_ERRTXT_IO   "OSSP rc:Failure, input output.\n"
#define RC_ERRTXT_INT  "OSSP rc:Failure, internal problem.\n"
#define RC_ERRTXT_FNC  "OSSP rc:Failure, function file %s could not be opened.\n"
#define RC_ERRTXT_LOC  "OSSP rc:Failure, location dir %s could not be opened.\n"
#define RC_ERRTXT_TMP  "OSSP rc:Failure, temporary dir %s could not be opened.\n"
#define RC_ERRTXT_RCF  "OSSP rc:Failure, one or more rc files could not be opened.\n"
#define RC_ERRTXT_CFG  "OSSP rc:Failure, configuration problem.\n"
#define RC_ERRTXT_ROOT "OSSP rc:Failure, you must be root to run these commands.\n"
#define RC_ERRTXT_GEN  "OSSP rc:Failure, generic problem of class '%s',.\n"

/* Option friendly names, should not include newline termination */
#define RC_USE_NAME "usage"
#define RC_DBG_NAME "debug"
#define RC_VER_NAME "version"
#define RC_EVL_NAME "eval"
#define RC_HLP_NAME "help"
#define RC_INF_NAME "info"
#define RC_LBL_NAME "labels"
#define RC_PRN_NAME "print"
#define RC_PAR_NAME "parse"
#define RC_SIL_NAME "silent"
#define RC_RAW_NAME "raw"
#define RC_VRB_NAME "verbose"
#define RC_EXC_NAME "exec"
                             
#define RC_LOC_NAME "locate"
#define RC_CNF_NAME "conf"
#define RC_FNC_NAME "func"
#define RC_QRY_NAME "query"
#define RC_TMP_NAME "tmp"
                    
#define RC_OWN_NAME "RequireOwner"
#define RC_GRP_NAME "RequireGroup"      
#define RC_MSK_NAME "RequireUmask"      
#define RC_ASS_NAME "ParseEnvAss"       
#define RC_DEF_NAME "ParseSectionDef"   
#define RC_REF_NAME "ParseSectionRef"   
#define RC_PRM_NAME "ParseSectionParam" 
#define RC_TRM_NAME "ParseTerminal"     
#define RC_NCF_NAME "NameConfig"        
#define RC_CMN_NAME "NameCommon"        
#define RC_DFL_NAME "NameDefault"       
#define RC_ERR_NAME "NameError"

/* Error and warning strings, should not include newline termination */
#define RC_ERRSTR_OK   "Okay"
#define RC_ERRSTR_USE  "Usage"
#define RC_ERRSTR_MEM  "Memory"
#define RC_ERRSTR_SYS  "System"
#define RC_ERRSTR_IO   "Input/Output"
#define RC_ERRSTR_INT  "Internal"
#define RC_ERRSTR_FNC  "Funcfile"
#define RC_ERRSTR_LOC  "Location"
#define RC_ERRSTR_TMP  "Tempdir"
#define RC_ERRSTR_RCF  "Rcfile"
#define RC_ERRSTR_TRM  "Terminate"
#define RC_ERRSTR_CFG  "Config/Options"
#define RC_ERRSTR_ROOT "Permissions"
#define RC_ERRSTR_UNK  "Unrecognized"
#define RC_WRNSTR_OWR  "Overwrite"
#define RC_WRNSTR_NUL  "NULLPointer"

#define RC_GLOB_WILD  "all"

/* Nontranslatable machine strings, do not translate */
#define RC_ECHO_STR   "echo "
#define RC_BANG_STR   "#! /bin/sh\n"

/* Default values, do not include newlines and special chars must be escaped */
#define RC_DEF_ON     "1"                       /* Digital switch */
#define RC_DEF_OFF    "0"                       /* Digital switch */
#define RC_DEF_TMP    "/tmp"                    /* Temporary directory name */
#define RC_DEF_DEF    "^%(\\w+)[ \t]*(.*?)\\n(.*?)^$" /* Section definition */
#define RC_DEF_NCF    "config"                  /* Config section name */
#define RC_DEF_CMN    "common"                  /* Common section name */
#define RC_DEF_UIG    "-u"                      /* Section user string */
#define RC_DEF_UID    -1                        /* Section user value  */
#define RC_DEF_PRG    "-p"                      /* Section priority string */
#define RC_DEF_PRI    200                       /* Section priority value */

/* Handling temporary and output during evaluation mode */
#define RC_EVL_TMP    "rc.XXXXXX"                            /* Temporary file template   */
#define RC_EVL_SUF    ".tmp"                                 /* Temporary file suffix     */
#define RC_EVL_OUT    ". %s; rm -f %s 2>/dev/null || true\n" /* Command line output       */
#define RC_EVL_DBG    ". %s\n"                               /* Command line debug output */

#endif /* __OSSPRC_CONST_H__ */
