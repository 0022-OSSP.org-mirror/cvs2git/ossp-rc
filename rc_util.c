/*  OSSP rc - Run-Command Processor
**  Copyright (c) 2002-2003 Ralf S. Engelschall
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland GmbH
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP rc, a portable run-command processor
**  which can be found at http://www.ossp.org/pkg/lib/rc/
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  rc_util.c: Run-Command Processor ISO C source file
*/

#include <stdlib.h>
#include <string.h>

#include "rc.h"         /* Error definitions  */
#include "rc_const.h"   /* String definitions */


/* Translate an error code to a string */
char *strErr(rc_return_t rv)
{
    if      (rv == RC_OK)       return RC_ERRSTR_OK;
    else if (rv == RC_ERR_USE)  return RC_ERRSTR_USE;
    else if (rv == RC_ERR_MEM)  return RC_ERRSTR_MEM;
    else if (rv == RC_ERR_SYS)  return RC_ERRSTR_SYS;
    else if (rv == RC_ERR_IO)   return RC_ERRSTR_IO;
    else if (rv == RC_ERR_INT)  return RC_ERRSTR_INT;
    else if (rv == RC_ERR_FNC)  return RC_ERRSTR_FNC;
    else if (rv == RC_ERR_LOC)  return RC_ERRSTR_LOC;
    else if (rv == RC_ERR_TMP)  return RC_ERRSTR_TMP;
    else if (rv == RC_ERR_RCF)  return RC_ERRSTR_RCF;
    else if (rv == RC_ERR_TRM)  return RC_ERRSTR_TRM;
    else if (rv == RC_ERR_CFG)  return RC_ERRSTR_CFG;
    else if (rv == RC_ERR_ROOT) return RC_ERRSTR_ROOT;
    else if (rv == RC_WRN_OWR)  return RC_WRNSTR_OWR;
    else if (rv == RC_WRN_NUL)  return RC_WRNSTR_NUL;
    else                        return RC_ERRSTR_UNK;
}

/* Vector copy constructor */
char **vectorCopy(const char **kpszVec)
{
    int nSecs  = 0;
    int nIndex = 0;
    size_t nBytes  = 0;
    char **pszTemp = NULL;

    if (kpszVec) {
        for (nSecs = 0; kpszVec[nSecs]; nSecs++);   /* Count the sections */
        pszTemp = malloc(sizeof (char **) * nSecs + 1);

        for (nIndex = 0; kpszVec[nIndex]; nIndex++) {           /* Copy loop */
            nBytes = (strlen(kpszVec[nIndex]) + 1) * sizeof(char); /* for each */
            pszTemp[nIndex] = malloc(nBytes);                   /* element   */
            memcpy(pszTemp[nIndex], kpszVec[nIndex], nBytes);   /* in vector */
        }
        pszTemp[nIndex] = NULL; /* Used later to find the end of the array */
        return(pszTemp);        /* Success */
    }
    else
        RC_THROW(RC_ERR_USE);   /* Incoming vector is NULL? */

    return(NULL);               /* Failure */
}

/* Vector counter */
short vectorCount(const char **pszVec)
{
    int i = 0;
    assert(pszVec);                 /* Check for unallocated incoming */

    while (pszVec[i])               /* Loop through elements,  */
        i++;                        /* and count them here     */

    return(i);
}

/* Vector destructor */
rc_return_t vectorDel(char **pszVec)
{
    int i;
    assert(pszVec);                 /* Check for unallocated incoming */

    for (i = 0; pszVec[i]; i++) {   /* Loop through elements   */
        free(pszVec[i]);            /* of vector, deallocating */
        pszVec[i] = NULL;           /* along the way           */
    }

    free(pszVec);                   /* Free the vector itself  */
    return(RC_THROW(RC_OK));
}

/* Section priority compare, to use with qsort(3) */
int priCompare(const void *pkv1, const void *pkv2)
{
    int nOne = 0;
    int nTwo = 0;

    if (pkv1) {
        nOne = ((rc_section_t *)pkv1)->m_nPri;
        if (pkv2)
            nTwo = ((rc_section_t *)pkv2)->m_nPri;
        else
            return (-1);
    }
    else if (pkv2)
        return (1);
    else
        return (0);

    if (nOne)
        if (nTwo)
            if (*(int *)nOne > *(int *)nTwo)
                return (1);
            else if (*(int *)nOne < *(int *)nTwo)
                return (-1);
            else
                return (0);
        else
            return (-1);
    else if (nTwo)
        return (1);
    else
        return (0);
}

