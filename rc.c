/*  OSSP rc - Run-Command Processor
**  Copyright (c) 2002-2003 Ralf S. Engelschall
**  Copyright (c) 2002-2003 Cable & Wireless Deutschland GmbH
**  Copyright (c) 2002-2003 The OSSP Project <http://www.ossp.org/>
**
**  This file is part of OSSP rc, a portable run-command processor
**  which can be found at http://www.ossp.org/pkg/lib/rc/
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  rc.c: Run-Command Processor ISO C source file
*/

#include <stdlib.h> /* Just for calling 'exit(3)' once */
#include "rc.h"     /* Public interfaces */


/************************************************
* main(int, char **)                            *
* Main rc control block                         *
************************************************/
int main(int argc, char *argv[])
{
    ex_t Except;
    rc_config_t *pConf = NULL;
    rc_proc_t *pProc = NULL;

    try {                       /* Configuration block, config is built here  */
        pConf = configNew();    /* Construct a new config, add default values */
        configLoad(pConf, argc, (const char **)argv); /* Load cli, env, conf  */
        configDefaults(pConf);  /* Add default values to empty config members */
    }
    catch(Except)               /* Exceptions of the configuration block      */
        rcError(Except);

    try {                       /* Main processing block, script built */
        pProc = procNew();      /* Construct a new processor           */
        procPopulate(pProc);    /* Populate with run commands          */
        procRun(pProc);         /* [Execute|Evaluate|Print] script     */
        procDelete(pProc);      /* Destroy the processor               */
        configDelete(pConf);    /* Destroy the configuration           */
    }
    catch(Except)               /* Exceptions while script processing  */
        rcError(Except);

    exit(0);                    /* Return success */
}

